package org.pub.pt.data.sources.domain;

/**
 * Abstract class that holds methods used by all or at least most of the {@link IDataSource} 
 * implementations.
 * @author balhau
 *
 */
public abstract class AbstractDataSource  implements IDataSource{
}
