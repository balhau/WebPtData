package org.pub.data.sources.sports.football.soccerway.domain.events;

/**
 * Created by vitorfernandes on 9/24/16.
 */
public enum MatchEventType {
    GOAL,ASSISTENCE
}
