package org.pub.global.configs;

/**
 * Object holding several global configurations needed for most of the data sources implementations
 * @author balhau
 *
 */
public class GlobalConfigs {
	public static final String USER_AGENT="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/43.0.2357.81 Chrome/43.0.2357.81 Safari/537.36";
	public static final int CONNECTION_TIMEOUT=120*1000;
}
